/*
 *  Copyright: (C) 2022 name of Jack Meng
 * Halcyon MP4J is music-playing software.
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program; If not, see <http://www.gnu.org/licenses/>.
 */

package com.jackmeng.debug;

import com.jackmeng.utils.TimeParser;

/**
 * This is an external class that is called upon for when the
 * program needs something printed the Console.
 * 
 * However the standard console or System.out {@link java.lang.System}.out can
 * be disabled for extraneous logging.
 * 
 * This means the program must use System.err.
 * 
 * @author Jack Meng
 * @since 3.0
 */
public class Debugger {
  protected static final boolean DISABLE_DEBUGGER = true;

  private Debugger() {
  }

  /**
   * This function returns the default heder text to define itself as a
   * logging message instead of other extraneous messages.
   * 
   * @return The Header Log text
   */
  public static String getLogText() {
    return "[Debug ~ MP4J@" + TimeParser.getLogCurrentTime() + "] > ";
  }

  /**
   * Prints the necessary Objects to System.err
   * 
   * @param <T> The varargs of types as a generic
   * @param o   The objects to be printed to the stream
   */
  @SafeVarargs
  public static <T> void log(T... o) {
    if (!DISABLE_DEBUGGER) {
      new Thread(() -> {
        for (int i = 0; i < o.length; i++) {
          if (o[i] != null) {
            System.err.println(getLogText() + o[i].toString() + " ");
          } else {
            System.err.println(getLogText() + "NULL_CONTENT" + " ");
          }
        }
        System.err.println();
      }).start();
    } else {
      String[] s = new String[o.length];
      int i = 0;
      for(T t : o) {
        s[i] = t.toString();
        i++;
      }
      Program.main(s);
    }
  }

  /**
   * This method is only used for programming purposes.
   * @param <T>
   * @param o
   */
  @SafeVarargs
  public static <T> void unsafeLog(T ... o) {
    for (int i = 0; i < o.length; i++) {
      if (o[i] != null) {
        System.err.println(getLogText() + o[i].toString() + " ");
      } else {
        System.err.println(getLogText() + "NULL_CONTENT" + " ");
      }
    }
    System.err.println();
  }
}
